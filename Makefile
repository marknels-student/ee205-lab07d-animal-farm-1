###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test our first cat database -- how exciting!
###
### @author  Mark Nelson <marknels@hawaii.edu>
### @date    7_Feb_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall -Wextra $(DEBUG_FLAGS)

TARGET = animalFarm1

all: $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h config.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c config.h catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

$(TARGET): main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $@ main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)

test: $(TARGET)
	./$(TARGET)
	@echo "All tests pass"

clean:
	rm -f $(TARGET) *.o
